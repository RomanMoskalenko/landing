$(function() {

})

// ------------------ Slider Implementation --------------

let sliderItems = document.querySelectorAll('.slider__item'),
    arrowLeft = document.querySelector('#arrow-left'),
    arrowRight = document.querySelector('#arrow-right'),
    current = 0;

//clear all images
function reset() {
    for (let i = 0; i < sliderItems.length; i++) {
        sliderItems[i].style.display = 'none';
    }
}

// Init slider
function startSlide() {
    reset();
    sliderItems[0].style.display = 'block';
}

// Show prev
function slideLeft() {
    reset();
    sliderItems[current - 1].style.display = 'block';
    current--;
}

// Show next
function slideRight() {
    reset();
    sliderItems[current + 1].style.display = 'block';
    current++;
}

// Left arrow click
arrowLeft.addEventListener('click', function() {
    if (current == 0) {
        current = sliderItems.length;
    }
    slideLeft();
});

// Right arrow click
arrowRight.addEventListener('click', function() {
    if (current == sliderItems.length) {
        current = -1;
    }
    slideRight();
});

startSlide();